<?php
/**
 * Trezo Soluções Web
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.trezo.com.br for more information.
 *
 * @category Trezo
 * @package CancelOverdueBankSlipOrder
 *
 * @copyright Copyright (c) 2017 Trezo Soluções Web. (https://www.trezo.com.br)
 *
 * @author Trezo Core Team <contato@trezo.com.br>
 */

class Trezo_CancelExpiredOrder_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * @return mixed
     */
    public function getFieldNameValue($filedName)
    {
        return Mage::getStoreConfig('cancelexpiredorder/general/'.$filedName);
    }

    public function isActive()
    {
        return Mage::getStoreConfig('cancelexpiredorder/general/active');
    }

    public function isOverdue($dateLimit)
    {
        $currentDate = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        if (strtotime($dateLimit) < strtotime($currentDate)) {
            return true;
        }
        return false;
    }
}

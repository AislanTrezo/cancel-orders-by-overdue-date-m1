<?php
/**
 * Trezo Soluções Web
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.trezo.com.br for more information.
 *
 * @category Trezo
 * @package CancelOverdueBankSlipOrder
 *
 * @copyright Copyright (c) 2017 Trezo Soluções Web. (https://www.trezo.com.br)
 *
 * @author Trezo Core Team <contato@trezo.com.br>
 */

class Trezo_CancelExpiredOrder_Model_Observer
{
    /**
     * @return array
     */

    public function getFieldNameValue($value){
        return Mage::helper('cancelexpiredorder')->getFieldNameValue($value);
    }

    public function isActive(){
        return Mage::helper('cancelexpiredorder')->isActive();
    }

    public function isOverdue($date){
        return Mage::helper('cancelexpiredorder')->isOverdue($date);
    }

    public function getOrderStatus()
    {
        $paymentMethodsToValidate = explode("," , $this->getFieldNameValue('payment_methods'));
        $orderStatusToValidate = explode("," , $this->getFieldNameValue('order_status'));
        $orders = Mage::getModel('sales/order_payment')
            ->getCollection()
            ->addFieldToFilter('method', $paymentMethodsToValidate)
            ->addFieldToFilter('status', $orderStatusToValidate);
        $table_prefix = Mage::getConfig()->getTablePrefix();
        $order_table = $table_prefix . 'sales_flat_order';
        $on_condition = "main_table.parent_id = $order_table.entity_id";
        $orders->getSelect()
            ->join($order_table , $on_condition);
        return $orders;
    }

    /**
     *
     */
    public function cancelOverdueOrder()
    {
        if (!$this->isActive()) {
            return;
        }
        foreach ($this->getOrderStatus()->getItems() as $order_payment) {
            $dateLimit = date('Y-m-d H:i:s' , strtotime($order_payment->getData('created_at') .' + '. $this->getFieldNameValue('max_overdue_days') .' days'));
            if ($this->isActive() && $this->isOverdue($dateLimit)) {
                $order = Mage::getModel('sales/order')->load($order_payment->getData('entity_id'));
                if ($order->canCancel()) {
                    try {
                        $order->cancel();
                        $order->setStatus(Mage_Sales_Model_Order::STATE_CANCELED);
                        $order->setState(Mage_Sales_Model_Order::STATE_CANCELED);
                        $order->addStatusHistoryComment(Mage::helper('cancelexpiredorder')->__('The payment has expired.'), true);
                        $order->save();
                    } catch (Exception $e) {
                        Mage::logException($e);
                    }
                }
            }
        }
    }
}

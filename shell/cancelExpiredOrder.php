<?php
/**
 * Trezo Soluções Web
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.trezo.com.br for more information.
 *
 * @category Trezo
 * @package CancelOverdueBankSlipOrder
 *
 * @copyright Copyright (c) 2017 Trezo Soluções Web. (https://www.trezo.com.br)
 *
 * @author Trezo Core Team <contato@trezo.com.br>
 */

require_once 'abstract.php';

class Trezo_Cancel_Expired_Order extends Mage_Shell_Abstract
{
    public function run()
    {
        Mage::getModel('cancelexpiredorder/observer')->cancelOverdueOrder();
    }
}

$shell = new Trezo_Cancel_Expired_Order();
$shell->run();
